console.log('Hey')

//An object is a collection of related data and/or functionality.
//CSS - everything is a box
//JS - most things are objects.
//Cellphone is an object in the real world.
	//It has its own properties (color, weight, unit)
	//It has its own functions (open, close, send messages, notes, incoming, outgoing)
	//How to create/instantiate an object in javascript
	//There are 2 ways to create an object.
	//1. Curly braces
//let cellphone = {}
	//2. Object Initializer
//let cellphone = Object()

// let cellphone = {
// 	color: "White",
// 	weight: "105g",
// 	//an object can also contain functions
// 	alarm: function() {
// 		console.log('Alarm is buzzing');
// 	},
// 	ring: function() {
// 		console.log('Cellphone is ringing');
// 	}
// }

let cellphone = Object({
	color: "Orange",
	weight: "105g",
	alarm: function() {
		console.log('Bzz bzz bzz');
	},
	ring: function() {
		console.log('Ring ring ring');
	}
})

console.log(cellphone);

//Babel is an example of a transpiler
//How to create an object as a blueprint
	//We can create reusable functions that will create several objects that have the same data structures.

//Syntax: function DesiredBlueprintName (argN) {}
//this.argN = valueNiArgN

function Laptop(name, manufactureDate, color) {
	this.cutie = name;
	this.createdOn = manufactureDate;
	this.kulay = color;
}
// this - keyword will allow us to assign a new object properties by associating the received values.
// new keyword is used to create an instance of a new object.
let item1 = new Laptop('Acer', '2020', "Graphite Blue");
let item2 = new Laptop('HP', '2019', 'Gray');

console.log(item1);
console.log(item2);

function PokemonAnatomy(name, type, level, health) {
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonType = type;
	this.pokeHealth = 80 * level;
	this.attack = function() {
		console.log('Pokemon Tackle');
	}
}

let pikachu = new PokemonAnatomy('Pikachu', 'Electric', 3);
let ratata = new PokemonAnatomy('Ratata', 'Ground', 2);
let meowth = new PokemonAnatomy('Meowth', 'Normal', 9);
let snorlax = new PokemonAnatomy('Snorlax', 'Normal', 9);
let jigglypuff = new PokemonAnatomy('Jigglypuff', 'Fairy', 7);

console.log(jigglypuff);

//Access Elements inside an object
//1.Dot notation(.)
console.log(snorlax.pokemonLevel);
//2. Square brackets []
console.log(meowth['pokeHealth']);